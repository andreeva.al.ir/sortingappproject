package com.example.sortingapp.service;

import org.apache.log4j.Logger;

import java.util.Arrays;

/**
 * Simple class for sorting array of numbers
 */
public class SortingService {
    private static final Logger log = Logger.getLogger(SortingService.class);

    /**
     * Method sorts an array of int.
     * ana returns sorted array of int.
     */
    public int[] sort(int[] numbers) {
        log.info(String.format("Array before sorting: %s", Arrays.toString(numbers)));
        Arrays.sort(numbers);
        log.info(String.format("Array after sorting: %s", Arrays.toString(numbers)));
        return numbers;
    }
}
