package com.example.sortingapp.controller;

import com.example.sortingapp.service.SortingService;
import org.apache.log4j.Logger;

/**
 * Controller is a simple class for parsing array to suitable format
 * and transfer it to the SortingService
 */
public class Controller {
    private final SortingService service = new SortingService();
    private static final Logger log = Logger.getLogger(Controller.class);

    /**
     * Method for parsing input array.
     *
     * @param args - array of String
     * @throws IllegalArgumentException – If it was null reference as argument
     *                                  If an array has 11 or more elements,
     *                                  if at least one array`s element can`t be represents as an Integer
     */
    public int[] parse(String[] args) {
        int[] result;
        try {
            if (args.length == 0) {
                return result = new int[0];
            }
            if (args.length >= 10) {
                throw new IllegalArgumentException();
            } else {
                result = new int[args.length];
            }
            int index = 0;
            for (String s : args) {
                result[index++] = (Integer.parseInt(s));
            }
        } catch (NumberFormatException f) {
            log.error("Unable to parse argument, not a number.", f);
            throw new IllegalArgumentException("Not a number", f);
        } catch (IllegalArgumentException e) {
            log.error("More than 10 elements, must be up to ten.", e);
            throw new IllegalArgumentException("More than 10 elements", e);
        }
        return result;
    }

    /**
     * Pass suitable array to SortingService for sort
     * and returns sorted array.
     *
     * @param numbers - array of numbers
     */
    public int[] sort(int[] numbers) {
        return service.sort(numbers);
    }
}
