package com.example.sortingapp;

import com.example.sortingapp.controller.Controller;

import org.apache.log4j.Logger;

import java.util.Arrays;

/**
 * SortingApplication is an entry point into application
 */
public class SortingApplication {
    private static final Logger log = Logger.getLogger(SortingApplication.class);

    public static void main(String[] args) {
        log.info(String.format("Application has started. Received args: %s", Arrays.toString(args)));
        Controller controller = new Controller();
        int[] array = controller.parse(args);
        printArray(controller.sort(array));
    }

    public static void printArray(int[] array) {
        for (int i : array) {
            System.out.print(i + " ");
        }
    }
}
