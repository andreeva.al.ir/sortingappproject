import com.example.sortingapp.service.SortingService;
import org.junit.Test;

import static org.junit.Assert.assertArrayEquals;

public class ServiceSortTest {
    SortingService service = new SortingService();


    @Test(expected = NullPointerException.class)
    public void testNullCase() {
        service.sort(null);
    }

    @Test
    public void testAlreadySortedArrayCase() {
        int[] expected = {0, 1, 2, 7, 11, 13, 26, 32, 44, 54, 56, 59, 67, 98, 123};
        int[] actual = {0, 1, 2, 7, 11, 13, 26, 32, 44, 54, 56, 59, 67, 98, 123};
        service.sort(actual);
        assertArrayEquals(expected, actual);
    }

    @Test
    public void testRegularArrayCase() {
        int[] expected = {0, 1, 2, 7, 11, 13, 26, 32, 44, 54, 56, 59, 67, 98, 123};
        int[] actual = {54, 67, 123, 2, 44, 32, 56, 13, 0, 98, 11, 7, 59, 26, 1};
        service.sort(actual);
        assertArrayEquals(expected, actual);
    }

    @Test
    public void testEmptyArrayCase() {
        int[] expected = new int[0];
        int[] actual = new int[0];
        service.sort(actual);
        assertArrayEquals(expected, actual);
    }

    @Test
    public void testSingleElementArrayCase() {
        int[] arr = {1};
        int[] arr1 = {1};
        service.sort(arr1);
        assertArrayEquals(arr, arr1);
    }
}
