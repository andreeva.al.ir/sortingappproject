import com.example.sortingapp.service.SortingService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertArrayEquals;

@RunWith(Parameterized.class)
public class ServiceSortParametrizedTest {
    int[] expected;
    int[] actual;
    SortingService service = new SortingService();

    public ServiceSortParametrizedTest(int[] expected, int[] actual) {
        this.expected = expected;
        this.actual = actual;
    }

    @Parameters
    public static List<Object[]> testCases() {
        return Arrays.asList(new Object[][]{
                {new int[]{1, 2, 3}, new int[]{2, 3, 1}},
                {new int[]{0, 0, 3}, new int[]{0, 3, 0}},
                {new int[]{1, 1, 1}, new int[]{1, 1, 1}},
                {new int[]{0, 1, 2, 3, 4, 5, 6, 7, 8, 9}, new int[]{2, 3, 1, 0, 8, 9, 5, 6, 7, 4}},
                {new int[]{-2, 0, 1, 32, 81, 300, 1200}, new int[]{0, 1, 300, 1200, -2, 81, 32}},
        });
    }

    @Test
    public void parametrizedTestRegularArrayCase() {
        service.sort(actual);
        assertArrayEquals(expected, actual);
    }
}
