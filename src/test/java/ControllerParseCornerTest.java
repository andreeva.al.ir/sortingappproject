import com.example.sortingapp.controller.Controller;
import org.junit.Test;

import static org.junit.Assert.assertArrayEquals;

public class ControllerParseCornerTest {
    Controller controller = new Controller();

    @Test(expected = IllegalArgumentException.class)
    public void testAbove10Case() {
        controller.parse(new String[]{"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11"});
    }

    @Test(expected = IllegalArgumentException.class)
    public void testInappropriateTypeCase() {
        String[] wrongArgument = {"a"};
        controller.parse(wrongArgument);
    }

    @Test
    public void testEmptyArrayCase() {
        int[] expected = new int[0];
        String[] actual = new String[0];
        assertArrayEquals(expected, controller.parse(actual));
    }
    @Test
    public void testOneArgumentCase() {
        int[] expected = {1};
        String[] actual = {"1"};
        assertArrayEquals(expected, controller.parse(actual));
    }
}
